const horseRaceGame = new HorseRacing();

function HorseRacing() {
    const PLAYER_DEFAULT_BANK = 1000;
    
    this.horses = [
        // заполните список лошадей
        'Искорка',
        'Флаттершай',
        'Пинки Пай',
        'Рарити',
        'Эплджек',
    ].map(name => new Horse(name));

    this.player = new Player(PLAYER_DEFAULT_BANK);
    this.wagers = [];
    this.clearWagers = function () {
        // очистить ставки
        this.wagers = [];
    };

    function Horse(name) {
        this.name = name;
        this.run = function () {
            // вернуть Promise, который будет автоматически выполнен через случайное время
            return new Promise(resolve => {
                const promiseTime = getRandomInt(500, 3000);
                setTimeout(() => resolve(this.name), promiseTime);
            });             
        };
    }

    function Player(bank) {
        this.account = bank;
        
    }

    this.playerAccountСhange = (value) => {
        this.player.account += value; 
    }

    this.isEnoughMoneyToBet = (sumToBet) => {
        if (this.player.account - sumToBet >= 0) {
            return true;
        }
        console.log(`У вас недостаточно денег для ставки. На счете осталось ${horseRaceGame.player.account} монеток`);
        return false;  
    }

    this.areWagersEmpty = () => {
        if (this.wagers.length === 0) {
            console.log('Ставки не были сделаны.');
            return true;
        } 
        return false;
    }

    this.isValidHorseName = (horseName) => {
        const result = this.horses.find(horse => horse.name === horseName);
        if (result !== undefined) {
            return true;
        }
        console.log('Лошадь с такой кличкой в скачках не участвует!');
        return false;
    }

    this.isValidValueOfBet = (sumToBet) => {
        const isNaturalNumber = typeof(sumToBet) === 'number' && sumToBet > 0;
        if (isNaturalNumber) {
            return true;
        } 
        console.log('Не верный формат ставки!');
        return  false;
    }



    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }

    // здесь вы можете дописать различные вспомогательные функции
    // например, для проверки ставок, изменения счета игрока и т.д.
}

function showHorses() {
    // вывести список лошадей
    horseRaceGame.horses.forEach((horse) => console.log(horse.name));
}

function showAccount() {
    // показать сумму на счету игрока
    console.log(`Сумма на счете игрока ${horseRaceGame.player.account} монеток`);
}

function setWager(name, sumToBet) {
    // сделать ставку на лошадь в следующем забеге
    if (horseRaceGame.isValidValueOfBet(sumToBet) &&
        horseRaceGame.isValidHorseName(name) &&
        horseRaceGame.isEnoughMoneyToBet(sumToBet)) {

        horseRaceGame.wagers.push({name, sumToBet});
        horseRaceGame.playerAccountСhange(-sumToBet);
        console.log(`Ставка на лошадь ${name} - ${sumToBet}`);
    }
}

function startRacing() {
    // начать забег
    if (horseRaceGame.areWagersEmpty()){
        return;
    }
    let winner = null;
    const promises = horseRaceGame.horses.map(horse => horse
        .run()
        .then((name) => {
            console.log(name); 
            return name;
        })
    );

    Promise.race(promises).then((name) => {
        winner = name;  
    });

    Promise.all(promises).then(() => {
        horseRaceGame.wagers.forEach(horse => {
            if (horse.name === winner) {
                horseRaceGame.playerAccountСhange(horse.sumToBet * 2);
                console.log(`Первым финишировал ${winner}`);
                console.log(`Вы выиграли ${horse.sumToBet * 2}!`);
            }
        });
        console.log(`Ваш счёт ${horseRaceGame.player.account}!`);
        horseRaceGame.clearWagers();
    });
}

function newGame() {
    // восстановить исходный счет игрока
    console.log('Новая игра!');
    this.player.account = horseRaceGame.PLAYER_DEFAULT_BANK;
    horseRaceGame.clearWagers();
}
